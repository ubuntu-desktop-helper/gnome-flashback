#include <gio/gio.h>

#if defined (__ELF__) && ( __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 6))
# define SECTION __attribute__ ((section (".gresource.gf"), aligned (8)))
#else
# define SECTION
#endif

static const SECTION union { const guint8 data[1537]; const double alignment; void * const ptr;}  gf_resource_data = {
  "\107\126\141\162\151\141\156\164\000\000\000\000\000\000\000\000"
  "\030\000\000\000\254\000\000\000\000\000\000\050\005\000\000\000"
  "\000\000\000\000\003\000\000\000\003\000\000\000\004\000\000\000"
  "\005\000\000\000\324\265\002\000\377\377\377\377\254\000\000\000"
  "\001\000\114\000\260\000\000\000\264\000\000\000\113\120\220\013"
  "\000\000\000\000\264\000\000\000\004\000\114\000\270\000\000\000"
  "\274\000\000\000\011\375\353\065\003\000\000\000\274\000\000\000"
  "\032\000\166\000\330\000\000\000\337\005\000\000\301\164\376\321"
  "\004\000\000\000\337\005\000\000\020\000\114\000\360\005\000\000"
  "\364\005\000\000\260\267\044\060\001\000\000\000\364\005\000\000"
  "\006\000\114\000\374\005\000\000\000\006\000\000\057\000\000\000"
  "\001\000\000\000\157\162\147\057\004\000\000\000\146\154\141\163"
  "\150\142\141\143\153\055\160\157\154\153\151\164\055\144\151\141"
  "\154\157\147\056\165\151\000\000\253\056\000\000\001\000\000\000"
  "\170\332\355\132\113\163\333\066\020\276\373\127\240\070\344\322"
  "\221\145\071\251\233\066\222\062\171\331\323\231\036\162\160\247"
  "\107\014\110\256\110\124\040\300\002\240\145\345\327\167\111\211"
  "\212\154\121\174\312\261\354\372\106\101\330\005\366\333\335\017"
  "\213\307\370\375\155\054\311\015\030\053\264\232\320\321\351\031"
  "\045\240\174\035\010\025\116\350\137\327\227\203\267\364\375\364"
  "\144\374\323\140\100\256\100\201\341\016\002\262\020\056\042\241"
  "\344\001\220\327\247\243\337\116\317\310\140\200\235\204\162\140"
  "\146\334\007\022\350\230\013\324\027\052\035\303\140\046\271\215"
  "\074\356\317\351\364\204\220\261\201\177\123\141\300\022\051\074"
  "\354\342\346\077\323\357\063\100\175\027\164\230\367\163\020\047"
  "\022\307\043\076\312\333\011\275\054\324\174\325\162\056\334\147"
  "\301\245\016\051\111\270\001\345\046\364\312\315\377\026\052\320"
  "\213\174\024\224\117\214\116\300\270\045\121\074\206\011\365\271"
  "\142\063\355\247\226\116\057\271\264\060\036\026\035\312\373\173"
  "\332\004\140\330\102\004\056\242\323\137\352\272\073\341\044\120"
  "\342\014\127\026\147\315\075\211\215\113\300\321\076\244\056\302"
  "\031\012\037\155\251\323\202\260\210\157\231\154\303\111\306\072"
  "\340\222\116\257\115\132\333\165\221\203\303\022\155\205\103\250"
  "\351\324\207\314\137\165\142\302\327\212\145\237\164\032\344\210"
  "\017\022\364\306\002\301\251\105\144\231\000\213\060\050\012\311"
  "\035\001\077\022\062\130\175\343\057\355\375\003\276\053\274\215"
  "\356\374\250\157\051\021\301\204\256\304\231\207\277\213\336\273"
  "\303\335\010\053\162\340\312\300\350\022\020\365\101\161\336\104"
  "\104\033\201\100\363\025\346\030\347\131\040\310\046\202\066\341"
  "\076\246\341\276\141\356\200\127\003\040\372\320\341\044\356\041"
  "\330\011\305\256\110\166\110\261\132\140\106\347\173\105\166\320"
  "\051\107\350\217\230\207\260\302\110\344\237\367\105\072\042\324"
  "\007\245\062\331\210\113\021\356\315\331\312\351\256\045\255\343"
  "\306\265\021\114\304\055\110\206\164\204\246\276\171\333\106\262"
  "\071\145\324\152\131\015\177\261\137\156\074\134\371\164\247\035"
  "\143\144\216\101\122\077\016\334\046\134\005\035\174\062\023\122"
  "\266\217\202\357\374\173\126\145\125\351\364\307\303\222\260\156"
  "\032\352\033\062\310\226\346\022\046\070\232\130\157\114\230\365"
  "\374\160\126\051\126\012\134\071\170\177\162\017\344\026\174\061"
  "\130\213\164\301\144\336\136\246\243\007\230\175\001\055\223\137"
  "\115\264\254\074\171\045\335\073\117\204\257\102\367\056\377\314"
  "\076\256\263\142\046\373\065\364\212\366\341\272\117\333\201\123"
  "\013\054\346\146\236\046\335\014\137\030\336\121\062\346\267\253"
  "\245\205\371\021\067\150\351\257\147\325\052\366\161\111\065\237"
  "\364\342\224\356\274\322\231\133\052\370\145\057\307\364\110\027"
  "\013\310\343\001\067\313\347\224\063\037\024\341\111\042\263\222"
  "\036\341\046\302\022\356\262\355\212\103\110\211\323\004\265\314"
  "\264\211\011\307\176\176\336\305\105\334\221\315\316\047\061\342"
  "\106\110\010\301\236\222\255\355\301\132\327\272\133\260\255\311"
  "\105\331\040\271\256\323\227\024\174\012\051\070\172\334\024\374"
  "\244\143\117\157\326\174\014\001\143\231\237\265\225\257\374\217"
  "\234\174\317\310\357\347\217\353\367\215\313\221\123\220\164\217"
  "\322\333\025\045\333\105\255\344\136\054\152\227\042\124\214\024"
  "\135\271\376\364\006\346\020\340\264\133\213\330\327\365\346\352"
  "\367\056\172\263\265\041\125\270\013\227\102\365\060\061\126\020"
  "\153\045\374\214\355\103\160\164\132\354\370\130\036\206\365\052"
  "\253\010\240\236\004\172\023\101\077\062\350\125\213\325\220\102"
  "\045\061\164\111\210\057\231\103\326\011\161\307\111\217\224\022"
  "\135\125\144\305\320\015\167\140\131\000\063\236\112\327\124\325"
  "\303\205\132\127\123\016\025\151\243\207\214\264\147\264\104\277"
  "\076\232\335\221\120\063\335\152\103\324\167\241\375\121\173\201"
  "\147\024\055\157\016\035\055\117\370\314\162\164\210\063\313\062"
  "\373\313\155\357\102\264\355\115\156\275\164\227\230\272\143\146"
  "\223\373\231\057\271\071\140\326\267\134\340\270\220\226\101\321"
  "\372\260\067\065\225\142\155\317\224\213\271\277\034\053\267\132"
  "\011\256\214\010\356\002\030\146\055\117\140\303\146\364\202\265"
  "\330\264\335\037\135\313\064\126\254\301\105\136\303\052\067\221"
  "\334\207\110\113\114\233\341\341\153\350\043\326\376\162\146\335"
  "\166\235\255\270\104\043\331\223\205\142\257\135\317\176\133\165"
  "\134\301\331\173\153\271\243\240\300\352\113\040\033\163\051\357"
  "\134\003\261\317\053\126\272\167\023\264\351\330\146\354\066\205"
  "\147\257\103\212\306\136\357\137\206\264\172\354\320\247\016\031"
  "\075\154\035\362\061\165\116\253\315\142\276\272\146\170\156\217"
  "\105\056\232\112\110\276\324\251\143\326\055\063\323\100\005\275"
  "\253\244\034\336\365\113\034\256\174\220\314\133\065\365\111\131"
  "\366\051\127\325\352\055\310\001\110\247\255\250\001\037\304\315"
  "\366\051\115\153\332\152\171\106\071\266\042\124\134\026\123\227"
  "\302\237\003\126\131\021\246\254\004\163\317\005\154\375\077\363"
  "\075\112\354\202\047\011\240\227\224\246\303\207\333\265\265\205"
  "\360\251\075\064\331\012\167\236\272\350\040\301\136\365\210\362"
  "\330\102\076\023\155\174\046\271\373\322\313\166\027\076\276\144"
  "\333\012\200\377\103\252\035\311\371\310\017\053\114\316\273\026"
  "\046\167\155\334\372\163\074\054\336\175\117\117\306\303\315\223"
  "\362\351\311\177\246\333\037\050\000\050\165\165\141\171\051\147"
  "\156\157\155\145\055\146\154\141\163\150\142\141\143\153\057\000"
  "\002\000\000\000\147\156\157\155\145\057\000\000\003\000\000\000"
  "" };

static GStaticResource static_resource = { gf_resource_data.data, sizeof (gf_resource_data.data) - 1 /* nul terminator */, NULL, NULL, NULL };

G_MODULE_EXPORT
GResource *gf_get_resource (void);
GResource *gf_get_resource (void)
{
  return g_static_resource_get_resource (&static_resource);
}
/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Modified by the GLib Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GLib Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GLib at ftp://ftp.gtk.org/pub/gtk/.
 */

#ifndef __G_CONSTRUCTOR_H__
#define __G_CONSTRUCTOR_H__

/*
  If G_HAS_CONSTRUCTORS is true then the compiler support *both* constructors and
  destructors, in a usable way, including e.g. on library unload. If not you're on
  your own.

  Some compilers need #pragma to handle this, which does not work with macros,
  so the way you need to use this is (for constructors):

  #ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
  #pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(my_constructor)
  #endif
  G_DEFINE_CONSTRUCTOR(my_constructor)
  static void my_constructor(void) {
   ...
  }

*/

#ifndef __GTK_DOC_IGNORE__

#if  __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR(_func) static void __attribute__((constructor)) _func (void);
#define G_DEFINE_DESTRUCTOR(_func) static void __attribute__((destructor)) _func (void);

#elif defined (_MSC_VER) && (_MSC_VER >= 1500)
/* Visual studio 2008 and later has _Pragma */

/*
 * Only try to include gslist.h if not already included via glib.h,
 * so that items using gconstructor.h outside of GLib (such as
 * GResources) continue to build properly.
 */
#ifndef __G_LIB_H__
#include "gslist.h"
#endif

#include <stdlib.h>

#define G_HAS_CONSTRUCTORS 1

/* We do some weird things to avoid the constructors being optimized
 * away on VS2015 if WholeProgramOptimization is enabled. First we
 * make a reference to the array from the wrapper to make sure its
 * references. Then we use a pragma to make sure the wrapper function
 * symbol is always included at the link stage. Also, the symbols
 * need to be extern (but not dllexport), even though they are not
 * really used from another object file.
 */

/* We need to account for differences between the mangling of symbols
 * for x86 and x64/ARM/ARM64 programs, as symbols on x86 are prefixed
 * with an underscore but symbols on x64/ARM/ARM64 are not.
 */
#ifdef _M_IX86
#define G_MSVC_SYMBOL_PREFIX "_"
#else
#define G_MSVC_SYMBOL_PREFIX ""
#endif

#define G_DEFINE_CONSTRUCTOR(_func) G_MSVC_CTOR (_func, G_MSVC_SYMBOL_PREFIX)
#define G_DEFINE_DESTRUCTOR(_func) G_MSVC_DTOR (_func, G_MSVC_SYMBOL_PREFIX)

#define G_MSVC_CTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _wrapper(void) { _func(); g_slist_find (NULL,  _array ## _func); return 0; } \
  __pragma(comment(linker,"/include:" _sym_prefix # _func "_wrapper")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _wrapper;

#define G_MSVC_DTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _constructor(void) { atexit (_func); g_slist_find (NULL,  _array ## _func); return 0; } \
   __pragma(comment(linker,"/include:" _sym_prefix # _func "_constructor")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _constructor;

#elif defined (_MSC_VER)

#define G_HAS_CONSTRUCTORS 1

/* Pre Visual studio 2008 must use #pragma section */
#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _wrapper(void) { _func(); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (*p)(void) = _func ## _wrapper;

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;

#elif defined(__SUNPRO_C)

/* This is not tested, but i believe it should work, based on:
 * http://opensource.apple.com/source/OpenSSL098/OpenSSL098-35/src/fips/fips_premain.c
 */

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  init(_func)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void);

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  fini(_func)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void);

#else

/* constructors not supported for this compiler */

#endif

#endif /* __GTK_DOC_IGNORE__ */
#endif /* __G_CONSTRUCTOR_H__ */

#ifdef G_HAS_CONSTRUCTORS

#ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(gfresource_constructor)
#endif
G_DEFINE_CONSTRUCTOR(gfresource_constructor)
#ifdef G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(gfresource_destructor)
#endif
G_DEFINE_DESTRUCTOR(gfresource_destructor)

#else
#warning "Constructor not supported on this compiler, linking in resources will not work"
#endif

static void gfresource_constructor (void)
{
  g_static_resource_init (&static_resource);
}

static void gfresource_destructor (void)
{
  g_static_resource_fini (&static_resource);
}
